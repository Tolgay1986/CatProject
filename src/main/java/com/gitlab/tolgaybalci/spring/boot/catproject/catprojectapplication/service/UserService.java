package com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;//Parolayı hashlemek için yazdık.

    /*Bu metot authentication için kullanıcı ismi ile kullanıcıyı döndürmeyi amaçlar.*/
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        return userRepository.findByUsername(username).get();
        //Optional bir tipden bir obje almak için get() kullanılır. Bu objeyi de userRepository'den alırız.
    }

    //Bu metodu parolayı hashleyerek veritabanına kaydetmek için yaparız.
    public void save(User user){
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);

        //user.getPassword() == password'e ulaşmak için, passwordEndcoder.encode() hashlemek için
        //user.setPasword() ise hashlenmis parolayı set etmek için kullanırız.
    }
}
