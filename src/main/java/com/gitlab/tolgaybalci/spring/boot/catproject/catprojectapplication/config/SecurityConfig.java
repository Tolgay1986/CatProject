package com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.config;

import com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.service.Role;
import com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{
    //Spring security configuration classı olduğunu söylemek için bu adapter a extends etmek gereklidir.

    @Autowired
    private UserService userService;

    @Bean
    public PasswordEncoder passwordEncoder(){

        return new BCryptPasswordEncoder();//Spring hash stardartı BCrypt, BCrypt bir hash standartıdır(SHA1, MD5,...)
        // Şifre parola değildir. Hashlemek şifrelemek(encryption) değildir. Şifrelemek çift taraflı bir fonksiyondur.(Encrypt, Dcrypt)
        //Hash geri döndürülemeyen tek taraflı bir fonksiyondur.
    }

    //Authentication yapmak için
    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {

        authenticationManagerBuilder.inMemoryAuthentication().withUser("Tolgay1986").password(passwordEncoder().encode("12345")).roles(Role.USER.toString());

        authenticationManagerBuilder.userDetailsService(userService).passwordEncoder(passwordEncoder());
        //Burada authentication manager için userservice i ve passwordencoder ı set ettik.
    }

    //Authorization yapmak için kullanılır
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception{
         httpSecurity.csrf().disable()//csrf siteler arası istek sahtekarlığı olarak bilinir.
        .authorizeRequests()
        .antMatchers("/static/**","/css/**", "/js/**", "/templates/**, /fonts/**").permitAll()//Kullanıcı sayfayı ve stillerini giriş yapmadan görebilmesi için kullanılır.
        .antMatchers("/register","/login","/h2-console/**", "/api/**").permitAll()
        .anyRequest().fullyAuthenticated()//Üsteki istekler dışında gelen isteklerin tamamen authenticate oması için kullanınlır
        .and()
        .formLogin()//login view ının hangi endpoint de tetiklenceğini söyler
        .loginPage("/login")
        .usernameParameter("username")//login view ında username inputunda kullanılan isim(name = attribute)
        .defaultSuccessUrl("/")//Kullanıcı başarılı bir şekilde login olduktan sonra kullanıcının yönlendirileceği endpoint
        .permitAll()
        .and()
        .logout()
        .logoutUrl("/logout")
        .permitAll()
        .and().httpBasic().realmName("MY_TEST_REALM").authenticationEntryPoint(getBasicAuthEntryPoint());

        httpSecurity.headers().frameOptions().disable();//H2 browser dan DB console kullanıyor. Bu DB console frame kullanıyor.
        //Spring security bu framelerin hepsini eziyor. Ezmemesi için frame optionlarını disable ediyoruz.
    }

    @Bean
    public CustomBasicAuthenticationEntryPoint getBasicAuthEntryPoint() {
        return new CustomBasicAuthenticationEntryPoint();
    }
}