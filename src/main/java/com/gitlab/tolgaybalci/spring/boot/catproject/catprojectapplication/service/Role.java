package com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.service;

public enum Role {

    USER, ADMIN;
}
