package com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.service;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class User implements UserDetails{//Custom sınıfımızı Spring Security mantığına uygun hale gelmesi için UserDetails implements ettik

    @Id
    @GeneratedValue
    private Long id;

    private String username;

    private String password;

    @Email
    private String email;

    private Boolean isAccountNonExpired = true;

    private Boolean isAccountNonLocked = true;

    private Boolean isCredentialsNonExpired = true;

    private Boolean isEnabled = true;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(Role.USER.toString());//Role.USER yaratılan her kullanıcının user olması için kullandık. Eğer admin deseydik her kişi admin oluğp sisteme müdahale edebileckti.
        grantedAuthorities.add(grantedAuthority);

        return grantedAuthorities;

        /*Kullanıcıya rol vermek için Spring Security'nin sağladığı GrantedAuthority(Role) tipinden bir liste tanımladık. Bu listeye
        daha önceden tanımladığımız Enum değerlerinden birini atadık. Atadığımız bu objeye listeye ekleyerek döndürdük.*/
    }

    /*İlk mettoda kulanıcının hesabnın süresi dolmuş mu, ikincisi hesabı kilitli mi, yetkisi dolmuş mu ve sonuncusu ise
    hesaba ulaşabiliyormuyuz diye gösterir.*/
    @Override
    public boolean isAccountNonExpired() {
        return isAccountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isAccountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return isCredentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }
}
