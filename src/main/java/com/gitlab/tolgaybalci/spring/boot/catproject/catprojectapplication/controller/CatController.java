package com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.controller;

import com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.domain.EyeColor;
import com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.domain.Gender;
import com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.repository.CatRepository;
import com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.Validator.CatValidator;
import com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.domain.Cat;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping("/cats")
public class CatController {

    @Autowired
    private CatRepository catRepository;

    @InitBinder
    protected void initBinder(WebDataBinder webDataBinder){
       webDataBinder.setValidator(new CatValidator());
    }

    @GetMapping("")
    public String getCatList(Model model, @RequestParam(name = "eyeColor", required = false) EyeColor eyeColor){
        Iterable<Cat> cats;

        if(eyeColor != null){
            cats = catRepository.findByEyeColor(eyeColor);
        }
        else{
            cats = catRepository.findAll();
        }


        /*İlgili butone tıklandığında URL de sadece o kısmın görünmesi için yapılır. İlk koşulda parametre boş değer değilse listeye
        repository ideki enum u bulduran metod gelir. Null ise listeye tüm datalar girer. Ayrıca her koşulda view kısmına data gönderillir.*/

        model.addAttribute("cats", cats);
        return "cats/CatList";
    }

    @GetMapping("/new")
    public String getNewCat(Model model){
        model.addAttribute("cat",new Cat());
        return "cats/NewCat";
    }

    @PostMapping("/new")
    public String postNewCat(@ModelAttribute @Valid Cat cat, BindingResult bindingResult){
        if(!bindingResult.hasErrors()){
            catRepository.save(cat);
            return "redirect:/cats";
        }
        else{
            return "cats/NewCat";
        }
    }

    @GetMapping("/{id}/update")
    public String getUpdateCat(@PathVariable Long id, Model model){
        if(catRepository.findById(id).isPresent()){
            model.addAttribute("cat", catRepository.findById(id).get());
            return "cats/UpdateCat";
        }
        else{
            log.warn("There is no such a cat {} with id", id);
            return "redirect:/cats";
        }
    }

    @PostMapping("/{id}/update")
    public String postUpdateCat(@ModelAttribute @Valid Cat cat, BindingResult bindingResult){
        if(!bindingResult.hasErrors()){
            catRepository.save(cat);
            return "redirect:/cats";
        }
        else{
            return "cats/UpdateCat";
        }
    }

    @PostMapping("/{id}/delete")
    public String postDeleteCat(@PathVariable Long id){
        if(catRepository.findById(id).isPresent()){
            catRepository.deleteById(id);
        }
        return "redirect:/cats";
    }
}
