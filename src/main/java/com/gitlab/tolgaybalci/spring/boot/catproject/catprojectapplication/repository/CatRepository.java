package com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.repository;

import com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.domain.Cat;
import com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.domain.EyeColor;
import com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.domain.Gender;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CatRepository extends CrudRepository<Cat, Long>{

    List<Cat> findByEyeColor(EyeColor eyeColor);
    List<Cat> findByGender(Gender gender);
}
