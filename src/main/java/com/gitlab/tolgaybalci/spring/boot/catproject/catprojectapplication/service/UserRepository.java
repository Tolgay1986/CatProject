package com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.service;

import com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.service.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

//Repository veritababnına doğrudan müdahale etmemek, geliştiriciyi veritabanından soyutlamak için kullanılan JPA'in sağladığı bir katmandır.
//ORM = Veritabanı server side bağlantısını sağlayan yapı
//JPA ise standart JAVA ORM Spesifikasyonudur.(Spesifikasyon örneği = Herhangi bir interface) Compile time çalışır.
//Hibernate JPA'in implementasyonudur. Kod run time da çalışır. "Hibernate implements JPA"
//@Repository annotasyonu springframework stereotype'ıdır.
//Stereotype ise spring tarafından sunulan hazır componentlerdir.
//Component depencdency injection yapmayı kolaylaştıran bir spring annotasyonudur.
//Dependency injection ise dependency pool da her objenin sadece bir instance'ını oluşturmak için kullandığımız yöntemdir.(Bakınız Singleton)
//Dependency injection ı kullanmasaydık her sorgu için bir instance yaratırdık.
//Dependecy pool application context içerisinde oluşturulan bağımlılık havuzudur.(Bakınız Object Pool Pattern)
//Application context ise run time da oluşan spring configuration bilgisidir.

@Repository
public interface UserRepository extends CrudRepository<User, Long>{

    Optional<User> findByUsername(String username);//null pointer exception yememek için optional olarak döndük.
}
