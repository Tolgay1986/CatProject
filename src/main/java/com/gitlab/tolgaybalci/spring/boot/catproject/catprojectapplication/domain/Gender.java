package com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.domain;

public enum Gender {

    MALE, FEMALE;
}
