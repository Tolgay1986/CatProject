package com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.domain;

public enum EyeColor {

    BLACK("BLACK"), GREEN("GREEN"), BLUE("BLUE"), YELLOW("YELLOW"), GRAY("GRAY");
    //cat.black kısmında key den value ya ulaşamıyor.(message.properties deki key in value suna )
    private final String message;

    EyeColor(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}