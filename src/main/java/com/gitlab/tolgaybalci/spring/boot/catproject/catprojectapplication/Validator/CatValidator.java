package com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.Validator;

import com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.domain.Cat;
import com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.domain.Gender;
import org.springframework.lang.Nullable;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class CatValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Cat.class.equals(aClass);
    }

    @Override
    public void validate(@Nullable Object o, Errors errors) {
        Cat c = (Cat) o;

        if(c.getAge() < 0){
            errors.rejectValue("age", "cat.is_not_equals_minus");
        }

        if(c.getColor().equals("")){
            errors.rejectValue("color", "cat.color_is_black");
        }

        if(c.isAlive() == false){
            errors.rejectValue("alive", "cat.is_not_alive");
        }

        if(c.getName().contains(" ")){
            errors.rejectValue("name", "cat.name_is_empty");
        }
    }
}
