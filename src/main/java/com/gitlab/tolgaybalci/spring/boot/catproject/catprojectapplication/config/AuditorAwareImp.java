package com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.config;

import com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.service.User;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.security.Principal;
import java.util.Optional;

public class AuditorAwareImp implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {

            return Optional.of(((UserDetails)principal).getUsername());

        } else {
            return Optional.of(principal.toString());
        }

        //return Optional.of(((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
    }

        //Entitiy'i olusturan User'ın username'ini Seucrity Context'den döndürmeyi sağlar.
}
