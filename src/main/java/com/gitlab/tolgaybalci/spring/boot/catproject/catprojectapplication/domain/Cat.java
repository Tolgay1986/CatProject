package com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.domain;

import com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication.service.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Cat {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty
    @Size(min = 1)
    private String name;

    @NotEmpty
    private String color;

    private Integer age;

    @NotNull
    private Gender gender = Gender.MALE;

    private boolean isAlive = true;

    @NotNull
    private EyeColor eyeColor = EyeColor.BLACK;

    @CreatedBy
    private String createdBy;

    @CreatedDate
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date CreatedDate;

    @CreatedBy
    private String lastModifiedBy;

    @CreatedDate
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date lastModifiedDate;
}
