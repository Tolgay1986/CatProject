package com.gitlab.tolgaybalci.spring.boot.catproject.catprojectapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatprojectapplicationApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatprojectapplicationApplication.class, args);
	}
}
